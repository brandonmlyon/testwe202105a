//https://stackoverflow.com/a/40638493
var myTimerObj = (function(document){
  var myTimer;

  function start() {
    myTimer = setInterval(myClock, 1000);
    var c = 0;

    function myClock() {
      document.getElementById("loadingNumerals").innerHTML = ++c;
      if (c == 100) {
        clearInterval(myTimer);
        alert("Complete");
      }
    }
  }
  
  function end() {
      clearInterval(myTimer)
  }

  return {start:start, end:end};
})(document);

var buttonState = "0";

var doThings = function()
{
  var toggleButton = document.getElementById('loaderToggle');
  var loadingUnit = document.getElementById('loadingUnit');
  toggleButton.onclick = function(e)
  {
    if (buttonState == "0")
    {
      myTimerObj.start();
      loadingUnit.textContent = "%";
      buttonState = "1";
      toggleButton.textContent = "Stop";

    } else if (buttonState == "1")
    {
      myTimerObj.end();
      buttonState = "0";
      toggleButton.textContent = "Start";
    };

  };
};

(function()
{
  doThings();
  console.log('ready');
}());
